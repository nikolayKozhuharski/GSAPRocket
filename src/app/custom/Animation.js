import svgPath from "../../config";
import gsap, { MotionPathPlugin } from "gsap/all";
gsap.registerPlugin(MotionPathPlugin);
export default class Animation {
  constructor() {
    this._rocketElement = document.querySelector(".rocket");
    this._backgroundElement = document.querySelector(".background");
    this._svgPath = svgPath.svgPath;
    this._rocketTween = null;
    this.tl = gsap.timeline();
  }

  async start() {
    this._rocketTween = this.createTween();
    let firstClick = true;
    this._backgroundElement.addEventListener("click", () => {
      if (firstClick) {
        console.log(this._rocketTween);
        this.tl.kill();
        this._rocketTween = null;
        firstClick = false;
      } else {
        this._rocketTween = this.createTween();
        this.tl.restart();
        firstClick = true;
      }
    });
  }
  createTween() {
    return this.tl.to(this._rocketElement, {
      motionPath: { path: this._svgPath, autoRotate: true },
      duration: 6,
      ease: "power4.inOut",
      repeat: -1,
    });
  }
}
